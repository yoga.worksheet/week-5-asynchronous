const searchField = document.getElementById("search-field");
const subtitle = document.getElementById("subtitle");
const searchButton = document.getElementById("search-button");
const sportsButton = document.getElementById('sports-button');
const entertainmentButton = document.getElementById('entertainment-button');
const technologyButton = document.getElementById('technology-button');
const topHeadlineButton = document.getElementById('top-headline-button');
const newsList = document.getElementById("news-list");

const API_KEY = "37b8dfed79c74b29abfe6f7cdb318db5";

window.addEventListener("load", () => {
	getNewsData(
		`https://newsapi.org/v2/top-headlines?country=id&apiKey=${API_KEY}`
	);
});

topHeadlineButton.addEventListener('click', () => {
	subtitle.textContent = "Top Headline News";
		getNewsData(
			`https://newsapi.org/v2/top-headlines?country=id&apiKey=${API_KEY}`
		);
})

sportsButton.addEventListener('click', () => {
	subtitle.textContent = "Sports Top Headlines";
		getNewsData(
			`https://newsapi.org/v2/top-headlines?country=id&category=sports&apiKey=${API_KEY}`
		);
})

entertainmentButton.addEventListener('click', () => {
	subtitle.textContent = "Entertainment Top Headlines";
		getNewsData(
			`https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=${API_KEY}`
		);
})

technologyButton.addEventListener('click', () => {
	subtitle.textContent = "Technology Top Headlines";
		getNewsData(
			`https://newsapi.org/v2/top-headlines?country=id&category=technology&apiKey=${API_KEY}`
		);
})

searchField.addEventListener("keypress", (event) => {
	if (event.key === "Enter") {
		event.preventDefault();
	}
});

searchButton.addEventListener("click", () => {
	let topic = searchField.value;
	if (topic === "") {
		subtitle.textContent = "Top Headline News";
		getNewsData(
			`https://newsapi.org/v2/top-headlines?country=id&apiKey=${API_KEY}`
		);
	} else {
		subtitle.textContent = `Search result for ${topic}`;
		getNewsData(
			`https://newsapi.org/v2/everything?q=${topic}&apiKey=${API_KEY}`
		);
	}
});

const getNewsData = (url) => {
	reset();
	newsList.innerHTML = `<div class="col"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>`;
	fetch(url)
		.then((result) => result.json())
		.then((response) => displayData(response))
		.catch((error) => `Error : ${error.message}`);
};

const displayData = (response) => {
	if (response.articles.length === 0) {
		newsList.innerHTML = `<div class='col-12'><h4>No Article Found</h4></div>`;
	} else {
		const result = response.articles.reduce((summary, item) => {
			return summary + makeCard(item);
		}, "");
		newsList.innerHTML = result;
	}
};

const reset = () => {
	newsList.innerHTML = "";
};

const dateFormat = (ISODate) => {
	const day = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
	const month = [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec",
	];
	const date = new Date(ISODate);
	return `${day[date.getDay()]}, ${date.getDate()} ${
		month[date.getMonth()]
	} ${date.getFullYear()}`;
};

const makeCard = (item) => {
	return `
	<div class="col">
		<div class="card h-100 border border-0 shadow">
			<img src="${item.urlToImage}" class="card-img-top" alt="..." />
			<div class="card-body">
				<h5 class="card-title">${item.title}</h5>
				<h6 class="card-subtitle mb-2 text-muted">By : ${item.author}</h6>
				<p class="card-text">
					${item.description}
				</p>
				<a href="${item.url}" class="align-self">Read More</a>
			</div>
			<div class="card-footer">
				<small class="text-muted"
					>Published at ${dateFormat(item.publishedAt)}</small
				>
			</div>
		</div>
	</div>
	`;
};
