import { Table } from "./DataTable.js";

const headData = ["ID", "Name", "Username", "Email", "Address", "Company"];
const root = document.getElementById("root");

let dummy = {
	headData: headData,
	bodyData: [],
};

const table = new Table(dummy);
table.render(root);

const request = (url, callback) => {
	const xhr = new XMLHttpRequest(),
		method = "GET",
		URL = url;

	xhr.responseType = "json";
	xhr.open(method, url, true);
	xhr.onreadystatechange = () => {
		if (xhr.readyState === XMLHttpRequest.DONE) {
			const status = xhr.status;
			if (status === 200) {
				return callback(xhr.response);
			} else {
				return "Error 404";
			}
		}
	};

	xhr.send();
};

request("https://jsonplaceholder.typicode.com/users", (response) =>
	displayData(response)
);

const displayData = (response) => {
	let responseArr = [];
	response.forEach((item) => {
		responseArr.push([
			item.id,
			item.name,
			item.username,
			item.email,
			`${item.address.street}, ${item.address.suite}, ${item.address.city}`,
			item.company.name,
		]);
	});
	table.changeData({
		...dummy,
		bodyData: responseArr,
	});
	table.render(root);
};
