export class Table {
	constructor(initialData) {
		this.data = initialData;
	}

    changeData(data) {
        this.data = data;
    }

	createHead() {
		let items = this.data.headData.reduce((result, current) => {
			return result + `<th>${current}</th>`;
		}, "");
		return `<thead><tr>${items}</tr></thead>`;
	}

	createBody() {
        if(this.data.bodyData.length === 0){
            return `<tbody><tr><td colspan="${this.data.headData.length}" class="text-center">Loading...</td></tr></tbody>`;
        }

		let items = this.data.bodyData.reduce((result, current) => {
			return (
				result +
				"<tr>" +
				current.reduce(
					(summary, item) => summary + `<td>${item}</td>`,
					""
				) +
				"</tr>"
			);
		}, "");
		return `<tbody>${items}</tbody>`;
	}

	render(target) {
		let displayTable = `<table class="table table-hover">${this.createHead()}${this.createBody()}</table>`;
		target.innerHTML = displayTable;
	}
}
